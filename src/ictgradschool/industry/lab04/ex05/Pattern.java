package ictgradschool.industry.lab04.ex05;

import java.security.PrivateKey;

public class Pattern {


    private final char symbol;
    private int number;

    public Pattern(int number, char symbol) {
        this.number = number;
        this.symbol = symbol;
    }

    public int getNumberOfCharacters() {
        return this.number;
    }

    public void setNumberOfCharacters(int numberOfCharacters) {
        this.number = numberOfCharacters;
    }

    @Override
    public String toString() {
        // todo: make a string, and add 'symbol' to it 'number' of times

        String line="";

        for (int i = 0; i < number; i++) {
            line += symbol;
        }


        // todo: return that string

        return line;
    }
}
