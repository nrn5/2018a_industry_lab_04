package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

import java.util.Objects;

/**
 * A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int PAPER = 2;
    public static final int SCISSORS = 3;
    String name = "";


    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        while (Objects.equals(name, "")) {
            System.out.print("Hi What is your name? " + name);
            name = Keyboard.readInput();
        }

        // todo: start loop here
        while (true) {

            if (!name.equals("")) {
                System.out.println("1. Rock");
                System.out.println("2. Scissors");
                System.out.println("3. Paper");
                System.out.println("4. Quit");
                System.out.print("Enter choice: ");
            }

            int choice = Integer.parseInt(Keyboard.readInput());
            int computerChoice = 0;
            computerChoice = (int) (Math.random() * 3 + 1);
            // todo: generate using math.random

            // todo: if player choice is to quit, return
            if (choice == 4) {
                System.out.println("Goodbye " + name + ". Thanks for playing :)");
                return;
            }
            // as detailed in the exercise sheet.
            displayPlayerChoice(name, choice);
            displayPlayerChoice("Computer", computerChoice);

            getResultString(choice, computerChoice);
        }
    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)

        if (choice == ROCK) {
            System.out.println(name + " chose rock.");
        } else if (choice == PAPER) {
            System.out.println(name + " chose paper.");
        } else if (choice == SCISSORS) {
            System.out.println(name + " chose scissors.");
        }
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if (playerChoice == computerChoice) {
            return true;
        }
        return false;
    }

    public void getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (playerChoice == PAPER && computerChoice == ROCK) {
            System.out.println(name + " wins because " + PAPER_WINS + ".");
        } else if (playerChoice == ROCK && computerChoice == SCISSORS) {
            System.out.println(name + " wins because " + ROCK_WINS + ".");
        } else if (playerChoice == SCISSORS && computerChoice == PAPER) {
            System.out.println(name + " wins because " + SCISSORS_WINS + ".");
        } else {
            System.out.println("No one wins. " + name + " you chose the same as the computer.");
        }
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
